import React, { useState, useEffect } from 'react';
import Card from './Card'

function ProductList() {

    const [list, setList] = useState([]);
    const [fav, setFav] = useState([]);
    const [basket, setBasket] = useState([]);

    const loadMore = async () => {

        try {
            const res = await fetch('./product.json');
            const data = await res.json();
            // const allFav = localStorage.getItem('Favorites');

            // data.forEach((item, index) => {
            //     if(allFav.indexOf(item.id) !== -1 ){
            //         item.inFavorite = true;
            //         data[index] = item;
            //     }
            // });
            setList(data);
        }

        catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        loadMore();
    }, [])

    // useEffect(() => {
    //     fetch('./product.json')
    //         .then(res => res.json())
    //         .then(data => {
    //             setList(data)
    //         })
    // }, [])


    useEffect(() => {
        if (fav.length > 0) {
            localStorage.setItem('Favorites', JSON.stringify(fav))
        }
    }, [fav])


    // const addToFav = (card) => {
    //     card.inFavorite = true;
    //     setFav(favIds => [...favIds, card.id]);
    //    // localStorage.setItem('Favorites', JSON.stringify(fav))
    // }

    const addToFav = (card) => {
        // card.active = false;
        setFav((favIds => [...favIds, card]))
        localStorage.setItem("Favorites", JSON.stringify(fav))
    }


    const removeFav = (card) => {
        const allFav = localStorage.getItem('Favorites');
        const allFavFilters = JSON.parse(allFav);
        setFav(allFavFilters.filter(item => item.id !== card.id));
    }

    useEffect(() => {
        if (basket.length > 0) {
            localStorage.setItem('Basket', JSON.stringify(basket))
        }
    }, [basket])

    const addBasket = (card) => {
        setBasket(idsInBasket => [...idsInBasket, card]);
    }

    return (
        <>
            <ul className='cards-container'>
                {
                    list.map(item =>
                        <Card
                            {...item}
                            key={item.id}
                            addToFav={addToFav}
                            removeFav={removeFav}
                            addBasket={addBasket}
                            card={item}
                        />
                    )}
            </ul>
        </>
    )

}

export default ProductList;