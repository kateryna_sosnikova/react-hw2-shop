import React from 'react';
import PropTypes from 'prop-types'

const Modal = ({ header, text, actions, closeButton, onClick}) => {

    return (
        <div className={'modal-wrap'} >
            <div className={'modal-overlay'} onClick={onClick}>
                <div className={'modal-content'} onClick={(e) => { e.stopPropagation() }}>

                    <div className={'modal-header'}>
                        <h2> {header} </h2>
                        <span className={'close'} onClick={onClick}>&times;</span>
                    </div>

                    <div className={'modal-body'}>
                        <h3>
                            {text}
                        </h3>
                    </div>

                    <div className="modal-footer">
                        {actions[0]}
                        {actions[1]}
                    </div>

                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.array,
    closeButton: PropTypes.bool, 
    onClick: PropTypes.func
}

export default Modal;